'use client'
import Link from "next/link"
import { usePathname } from "next/navigation";
import { CiBookmarkCheck } from "react-icons/ci"


interface Props {
  icon: React.ReactNode;
  path: string;
  title: string;
}

const SidebarItem = ({ icon, path, title }: Props) => {

  const pathName = usePathname();


  return (
    <div>
        <li>
            <Link href={ path } className={
              `px-4 py-3 flex items-center space-x-4 rounded-md text-gray-600 group
              hover:bg-gradient-to-r hover:bg-sky-700 hover:text-white
                ${ path === pathName ? `text-white bg-gradient-to-r from-sky-700 to-cyan400` : ``}
              `}>
                { icon }
              <span className="-mr-1 font-medium"> { title }</span>
            </Link>
        </li>
    </div>
  )
}

export default SidebarItem
