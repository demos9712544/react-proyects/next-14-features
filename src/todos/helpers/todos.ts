import { Todo } from "@prisma/client";


export const updateTodo = async( id: string, complete: boolean): Promise<Todo> => {

    const body = { complete };

    const todo = await fetch(`/api/all/${id}`, {
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then( resp => resp.json() );


    console.log( { todo })

    return todo;

};

export const createTodo = async( description: string): Promise<Todo> => {

    const body = { description };

    const todo = await fetch(`/api/all`, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then( resp => resp.json() );


    console.log( { todo })

    return todo;

};


export const deleteTodo = async (): Promise<boolean> => {

    await fetch(`/api/all`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then( resp => resp.json() );

    return true;
}
