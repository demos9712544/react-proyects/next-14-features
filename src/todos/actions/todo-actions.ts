'use server';

import prisma from "@/app/lib/prisma";
import { Todo } from "@prisma/client";
import { revalidatePath } from "next/cache";
import { NextResponse } from "next/server";



export const toggleTodo = async (id: string, complete: boolean): Promise<Todo> => {

    const todo = await prisma.todo.findFirst({where: { id }});

    if( !todo ) {
        throw `Todo with ${id} not find it`
    }

    const updateTodo = await prisma.todo.update({
        where: { id },
        data: { complete } 
    })

    // Revalidate the route I want to revalidate and reload only what I changed
    revalidatePath('/dashboard/server-actions')
    return updateTodo;
}


export const addTodo = async( description: string ) => {

    try {
        const todo = await prisma.todo.create({ data: { description } });
         // Revalidate the route I want to revalidate and reload only what I changed
        revalidatePath('/dashboard/server-actions')
        return todo;

    } catch (error) {
        return {
            message: `Failed to create todo`
        }
    }
}

export const deleteTodo = async(): Promise<void> => {
    await prisma.todo.deleteMany( { where : { complete: true }});
    // Revalidate the route I want to revalidate and reload only what I changed
    revalidatePath('/dashboard/server-actions')
}
