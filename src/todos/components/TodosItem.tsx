'use client'
import { Todo } from "@prisma/client"

import styles from './TodosItem.module.css' 
import { IoIosCheckboxOutline } from "react-icons/io"
import { IoSquareOutline } from "react-icons/io5"


interface Props {
  todo: Todo
  //acciones que quiero mandar a llamar
  toggleUpdate: (id: string, complete: boolean) => Promise<Todo|void>
}


const TodosItem = ({ todo, toggleUpdate }: Props) => {
  return (
    <div className={ todo.complete ? styles.todoDone : styles.todoPending }>
      <div className="flex sm-flex-row justify-start items-center gap-4">
        <div 
          onClick={() => toggleUpdate( todo.id, !todo.complete)}
          className={`
          flex p-2 rounded-md 
          cursor-pointer 
          hover:bg-opacity-60 
          bg-blue-100
          ${todo.complete ? 'bg-blue-100' : 'bg-red-100'}
          `
          }>
           {
              todo.complete ? <IoIosCheckboxOutline size={30} /> : <IoSquareOutline size={30}/>
           }
        </div>
        <div className="text-center sm:text-left">
          {todo.description}
        </div>
      </div>
    </div>
  )
}

export default TodosItem
