import prisma from "@/app/lib/prisma"
import { NewTodo } from "@/todos"
import TodosGrid from "@/todos/components/TodosGrid"

export const metadata = {
    title: 'List of Todos',
    desceiption: 'SEO title'
}

export default async function RestTodoPage() {

    const todos = await prisma.todo.findMany({ orderBy: { description: 'asc'} })

    return (
        <div>
            <h1 className="text-2xl ms-2">Rest API</h1>
            <div className="w-full px-3 py-5 mb-5 ms-8">
                <NewTodo />
            </div>
            <TodosGrid todos={ todos }/>
        </div>
    )
}