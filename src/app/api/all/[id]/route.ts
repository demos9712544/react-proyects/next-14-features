import prisma from "@/app/lib/prisma";
import { Todo } from "@prisma/client";
import { NextResponse, NextRequest } from "next/server";
import * as yup from "yup";

interface Segments {
    params: {
        id: string;
    }
}

const getAll = async( id: string ):Promise<Todo | null> => {

    const all = await prisma.todo.findFirst({ where: { id }});
    return all;

}


export async function GET(request: Request, {params}: Segments) {

    const all = await getAll( params.id)
     
    if(!all) {
        return NextResponse.json({message: `id ${ params.id } doesnt exist`}, { status: 404})
    }

    return NextResponse.json( all )
}


const putSchema = yup.object({
    complete: yup.boolean().optional(),
    description: yup.string().optional(),
})


export async function PUT( request: Request, {params} : Segments) {

    const all = await getAll( params.id)

    if(!all) {
        return NextResponse.json({message: `id ${ params.id } doesnt exist`}, { status: 404})
    }

    try {
        const { complete, description, ...rest } = await putSchema.validate( await request.json())
        
        const updatedAll = await prisma.todo.update( { 
            where: { id: params.id },
            data: { complete, description }
        } )
        
        return NextResponse.json(updatedAll, { status: 200})
        
    } catch (error) {
        return NextResponse.json( error, { status: 400})
    }
}