import prisma from "@/app/lib/prisma";
import { NextResponse, NextRequest } from "next/server";


export async function GET(request: Request) {

    await prisma.todo.deleteMany();

    await prisma.todo.createMany({
        data: [
            { description: 'Shoes', complete: true},
            { description: 't-shirt'},
            { description: 'Jumper'},
            { description: 'Jackets'},
            { description: 'Wet suit'},
        ]
    })

    return NextResponse.json({
        message: "Seed executed successfully"
    })
}