# Development

Steps to launch the application in developer

1. Build the database
```
docker compose up -d
```
2. I use Tableplus like database.
3. Rename the .env.template a .env
4. Replace enviroment variables.
5. install node modules ``` npm install ```
6. run ``` npm run dev ```
7. run those Prisma commands
``` 
npx prisma migrate dev
npx prisma generate
 ```
7. Run SEED to [create the local database](localhost:3000/api/seed).

#Prisma commands line
```
npx prisma init
npx prisma migrate dev
npx prisma generate
```


# Prod



# Stage